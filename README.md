# kubectl-docker

kubectl runtime client inside docker

Mount the kube config file to execute kubectl commands
`docker run -it --rm kubectl-docker -- kubectl cluster-info`
